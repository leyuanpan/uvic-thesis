%%
%% This is file `uvicthesis.cls',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% uvicthesis.dtx  (with options: `cls')
%% Copyright (C) 2016 by Leyuan Pan <leyuanpan@gmail.com>
%% 
%% This file is part of the UVIC-Thesis package project.
%% 
%% UVIC-Thesis is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%% 
%% UVIC-Thesis is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program. If not, see <http://www.gnu.org/licenses/>.

 \NeedsTeXFormat{LaTeX2e}[2015/01/01]
 \ProvidesClass{uvicthesis}[2016/09/10 0.7.0 The LaTeX template for thesis of the University of Victoria]
 \typeout{Document Class `uvicthesis' 0.7.0 by Leyuan Pan (2016/09/10)}
\newif\ifdoctordegree\doctordegreetrue
\newif\ifphdproposal\phdproposalfalse
\newif\ifmasterdegree\masterdegreefalse
\DeclareOption{doctor}{\doctordegreetrue\phdproposalfalse\masterdegreefalse}
\DeclareOption{proposal}{\doctordegreetrue\phdproposaltrue\masterdegreefalse}
\DeclareOption{master}{\doctordegreefalse\phdproposalfalse\masterdegreetrue}
\newif\ifnocolorlinks\nocolorlinksfalse
\DeclareOption{nocolorlinks}{\nocolorlinkstrue}
\newif\ifprint\printfalse\PassOptionsToClass{oneside}{book}
\DeclareOption{print}{\printtrue\PassOptionsToClass{twoside}{book}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}
\ProcessOptions
\LoadClass[a4paper,12pt]{book}
\RequirePackage{doc}
\RequirePackage{keyval}
\RequirePackage{ifthen}
\RequirePackage{graphicx}
\RequirePackage[dvipsnames]{xcolor}
\RequirePackage{indentfirst} % indent the first line
\RequirePackage{makeidx} % index
\RequirePackage[nottoc]{tocbibind}
\RequirePackage[numbers,sort&compress]{natbib} % modify reference format
\RequirePackage{array} % extend funcitons of array and tabular.
\RequirePackage{paralist}% List format
\setdefaultenum{1)}{a)}{i.}{A.}% List mode
\setdefaultleftmargin{4ex}{}{}{}{}{}
\RequirePackage{fancyhdr}
\RequirePackage{lettrine}
\newcommand{\UVICThesisPARstart}[2]{\lettrine[]{\textbf{#1}}{#2}}
\RequirePackage{amsthm}
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{proposition}{Proposition}
\newtheorem{corollary}{Corollary}
\newtheorem*{KL}{Klein's Lemma}

\theoremstyle{definition}
\newtheorem{definition}{Definition}[chapter]
\newtheorem{conjecture}{Conjecture}[chapter]
\newtheorem{example}{Example}[chapter]

\theoremstyle{remark}
\newtheorem{remark}{\bf{Remark}}
\newtheorem*{note}{Note}
\newtheorem{case}{Case}
\RequirePackage[cmex10]{amsmath}
\numberwithin{theorem}{chapter} % Reset theorem counter with every chapter
\numberwithin{lemma}{chapter} % Reset lemma counter with every chapter
\numberwithin{proposition}{chapter} % Reset proposition counter with every chapter
\numberwithin{corollary}{chapter} % Reset corollary counter with every chapter
\numberwithin{definition}{chapter} % Reset definition counter with every chapter
\numberwithin{remark}{chapter} % Reset remark counter with every chapter
\numberwithin{case}{chapter}%  Reset case counter with every chapter
\def\UVICQEDclosed{\mbox{\rule[0pt]{1.3ex}{1.3ex}}} % for a filled box
\def\UVICQED{\UVICQEDclosed}

\def\UVICproof{\@ifnextchar[{\@UVICproof}{\@UVICproof[\@UVICproofname]}}
\def\@UVICproof[#1]{\par\noindent\hspace{2em}{\itshape #1: }}
\def\endUVICproof{\hspace*{\fill}~\UVICQED\par}
\RequirePackage{setspace}
\onehalfspacing
\RequirePackage[breaklinks=true]{hyperref}
\AtBeginDocument{\indent
\InputIfFileExists{uvicthesis.cfg}
{\typeout{[uvicthesis]: Load uvicthesis.cfg successfully!}}
{\typeout{[uvicthesis]: Load uvicthesis.cfg failed!}}
\makeindex
}
\AtEndDocument{\backcover}
\ifprint
\RequirePackage[top=1.3in,bottom=1in,left=1.2in,right=1.2in,headsep=0.5in]{geometry}
\else
\RequirePackage[top=1.3in,bottom=1in,left=1.2in,right=1.2in,headsep=0.5in]{geometry}
\fi
\RequirePackage{titletoc}
\hypersetup{bookmarksnumbered=true}
\ifnocolorlinks
\hypersetup{colorlinks=false}
\else
\hypersetup{colorlinks=true}
\fi
\hypersetup{
linkcolor=blue,
anchorcolor=black,
citecolor=blue,
filecolor=magenta,
menucolor=red,
urlcolor=magenta
}
\newcommand{\clearpg}{
\ifprint
\cleardoublepage
\else
\clearpage
\fi
}
\renewcommand{\title}[1]{
\renewcommand{\@title}{#1}
}
\newif\ifsubtitle\subtitlefalse
\newcommand{\@subtitle}{This is Subtitle}
\newcommand{\subtitle}[1]{
\subtitletrue
\renewcommand{\@subtitle}{#1}
}
\renewcommand{\@author}{Author Name}
\renewcommand{\author}[1]{
\renewcommand{\@author}{#1}
}
\newcommand{\@department}{\@departmentinit}
\newcommand{\department}[1]{
\renewcommand{\@department}{#1}
}
\newcommand{\@university}{\@universityinit}
\newcommand{\university}[1]{
\renewcommand{\@university}{#1}
}
\newcommand{\@supervisor}{\@supervisorinit}
\newcommand{\supervisor}[1]{
\renewcommand{\@supervisor}{#1}
}
\newif\ifcosupervisor\cosupervisorfalse
\newcommand{\@cosupervisor}{\@cosupervisorinit}
\newcommand{\cosupervisor}[1]{
\cosupervisortrue
\renewcommand{\@cosupervisor}{#1}
}
\newif\ifdepartmentalmember\departmentalmemberfalse
\newcommand{\@departmentalmember}{\@departmentalmemberinit}
\newcommand{\departmentalmember}[1]{
\departmentalmembertrue
\renewcommand{\@departmentalmember}{#1}
}
\newif\ifdepartmentalmemberone\departmentalmemberonefalse
\newcommand{\@departmentalmemberone}{\@departmentalmemberoneinit}
\newcommand{\departmentalmemberone}[1]{
\departmentalmemberonetrue
\renewcommand{\@departmentalmemberone}{#1}
}
\newif\ifdepartmentalmembertwo\departmentalmembertwofalse
\newcommand{\@departmentalmembertwo}{\@departmentalmembertwoinit}
\newcommand{\departmentalmembertwo}[1]{
\departmentalmembertwotrue
\renewcommand{\@departmentalmembertwo}{#1}
}
\newif\ifdepartmentalmemberthree\departmentalmemberthreefalse
\newcommand{\@departmentalmemberthreename}{\@departmentalmemberthreeinit}
\newcommand{\departmentalmemberthree}[1]{
\departmentalmemberthreetrue
\renewcommand{\@departmentalmemberthree}{#1}
}
\newif\ifoutsidemember\outsidememberfalse
\newcommand{\@outsidemember}{\@outsidememberinit}
\newcommand{\@outsidememberorganization}{\@outsidememberinit~Organization}
\newcommand{\outsidemember}[2]{
\outsidemembertrue
\renewcommand{\@outsidemember}{#1}
\renewcommand{\@outsidememberorganization}{#2}
}
\newif\ifoutsidememberone\outsidememberonefalse
\newcommand{\@outsidememberone}{\@outsidememberoneinit}
\newcommand{\@outsidememberoneorganization}{\@outsidememberoneinit~Organization}
\newcommand{\outsidememberone}[2]{
\outsidememberonetrue
\renewcommand{\@outsidememberone}{#1}
\renewcommand{\@outsidememberoneorganization}{#2}
}
\newif\ifoutsidemembertwo\outsidemembertwofalse
\newcommand{\@outsidemembertwo}{\@outsidemembertwoinit}
\newcommand{\@outsidemembertwoorganization}{\@outsidemembertwoinit~Organization}
\newcommand{\outsidemembertwo}[2]{
\outsidemembertwotrue
\renewcommand{\@outsidemembertwo}{#1}
\renewcommand{\@outsidemembertwoorganization}{#2}
}
\newif\ifexternalmember\externalmemberfalse
\newcommand{\@externalmember}{\@externalmemberinit}
\newcommand{\@externalmemberorganization}{\@externalmemberinit~Organization}
\newcommand{\externalmember}[2]{
\externalmembertrue
\renewcommand{\@externalmember}{#1}
\renewcommand{\@externalmemberorganization}{#2}
}
\newif\ifexternalmemberone\externalmemberonefalse
\newcommand{\@externalmemberone}{\@externalmemberoneinit}
\newcommand{\@externalmemberoneorganization}{\@externalmemberoneinit~Organization}
\newcommand{\externalmemberone}[2]{
\externalmemberonetrue
\renewcommand{\@externalmemberone}{#1}
\renewcommand{\@externalmemberoneorganization}{#2}
}
\newif\ifexternalmembertwo\externalmembertwofalse
\newcommand{\@externalmembertwo}{\@externalmembertwoinit}
\newcommand{\@externalmembertwoorganization}{\@externalmembertwoinit~Organization}
\newcommand{\externalmembertwo}[2]{
\externalmembertwotrue
\renewcommand{\@externalmembertwo}{#1}
\renewcommand{\@externalmembertwoorganization}{#2}
}
\newcommand{\@degrees}{}
\newcommand{\degrees}[1]{
\renewcommand{\@degrees}{#1}
}
\newcommand{\@degree}{
\ifdoctordegree
\@doctordegree
\else
\@masterdegree
\fi
}
\newif\ifdegree\degreefalse
\newcommand{\degree}[1]{
\degreetrue
\renewcommand{\@degree}{#1}
}
\newcommand{\tpbreak}{\\[\baselineskip]}
\newcommand\panelist[3]{\noindent #1,~#2\\\noindent(#3)\tpbreak}
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}
\newcommand{\committeepanel}{
\HRule\\\panelist{\@supervisor}{\@supervisorinit}{\@department}
\ifcosupervisor
\HRule\\\panelist{\@cosupervisor}{\@cosupervisorinit}{\@department}
\else\fi
\ifdepartmentalmember
\HRule\\\panelist{\@departmentalmember}{\@departmentalmemberinit}{\@department}
\else\fi
\ifdepartmentalmemberone
\HRule\\\panelist{\@departmentalmemberone}{\@departmentalmemberoneinit}{\@department}
\else\fi
\ifdepartmentalmembertwo
\HRule\\\panelist{\@departmentalmembertwo}{\@departmentalmembertwoinit}{\@department}
\else\fi
\ifdepartmentalmemberthree
\HRule\\\panelist{\@departmentalmemberthree}{\@departmentalmemberthreeinit}{\@department}
\else\fi
\ifoutsidemember
\HRule\\\panelist{\@outsidemember}{\@outsidememberinit}{\@outsidememberorganization}
\else\fi
\ifoutsidememberone
\HRule\\\panelist{\@outsidememberone}{\@outsidememberoneinit}{\@outsidememberoneorganization}
\else\fi
\ifoutsidemembertwo
\HRule\\\panelist{\@outsidemembertwo}{\@outsidemembertwoinit}{\@outsidemembertwoorganization}
\else\fi
\ifexternalmember
\HRule\\\panelist{\@externalmember}{\@externalmemberinit}{\@externalmemberorganization}
\else\fi
\ifexternalmemberone
\HRule\\\panelist{\@externalmemberone}{\@externalmemberoneinit}{\@externalmemberoneorganization}
\else\fi
\ifexternalmembertwo
\HRule\\\panelist{\@externalmembertwo}{\@externalmembertwoinit}{\@externalmembertwoorganization}
\else\fi
}
\renewcommand{\maketitle}{
\markboth{}{}
\pagestyle{myheadings}
\pagenumbering{roman}
\fancypagestyle{plain}{
\fancyhf{}
\fancyhead[R]{\thepage}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
}
\coverpage
\committeepage
}
\newcommand{\coverpage}{
\thispagestyle{empty}
\begin{center}
{\large{\textbf{\@title}}}
\ifsubtitle
\\\@subtitle
\fi
\tpbreak
by
\tpbreak
\@author\\
\@degrees
\tpbreak

A
\ifphdproposal
Candidacy Proposal
\else
\ifdoctordegree
Dissertation
\else
Thesis
\fi
\fi
Submitted in Partial Fulfillment of the\\
Requirements for the Degree of
\tpbreak
\MakeUppercase{\@degree}
\tpbreak
in the \@department\\
\vfill
\copyright~\@author, \the\year\\
\@university
\tpbreak

\ifphdproposal
\@proposalcopyrightstatement
\else
\ifdoctordegree
\@copyrightstatement
\else
\@masterthesiscopyrightstatement
\fi
\fi
\end{center}
\pagebreak
\thispagestyle{empty}
\clearpg
}
\newcommand{\committeepage}{
\ifphdproposal
\thispagestyle{myheadings}
\begin{center}
\@title
\ifsubtitle
\\\@subtitle
\else\fi
\tpbreak
by
\tpbreak
\@author\\
\@degrees
\tpbreak
\end{center}
\vfill
\noindent\@supervisorycommitteetitle
\tpbreak
\committeepanel
\vfill
\addcontentsline{toc}{chapter}{\@supervisorycommitteetoc}
\pagebreak
\thispagestyle{empty}
\clearpg
\else
\thispagestyle{myheadings}
\begin{center}
\@title
\ifsubtitle
\\\@subtitle
\else\fi
\tpbreak
by
\tpbreak
\@author\\
\@degrees
\tpbreak
\end{center}
\vfill
\noindent\@supervisorycommitteetitle
\tpbreak
\committeepanel
\vfill
\addcontentsline{toc}{chapter}{\@supervisorycommitteetoc}
\pagebreak
\thispagestyle{empty}
\clearpg
\fi
}
\newenvironment{Abstract}{
\thispagestyle{myheadings}
\ifphdproposal
\else
\noindent\textbf{\@supervisorycommitteetitle}
\tpbreak
\committeepanel
\phantomsection
\fi
\addcontentsline{toc}{chapter}{\@abstracttoc}
\begin{center}
\textbf{\@abstracttitle}
\end{center}
\par
}{\newpage\thispagestyle{empty}\clearpg}
\newenvironment{Acknowledgement}{
\thispagestyle{myheadings}
\phantomsection
\addcontentsline{toc}{chapter}{\@acknowledgementtoc}
\begin{center}
{\large{\textbf{\@acknowledgementtitle}}}
\end{center}
\par
}{\newpage\thispagestyle{empty}\clearpg}
\newenvironment{Dedication}{
\thispagestyle{myheadings}
\phantomsection
\addcontentsline{toc}{chapter}{\@dedicationtoc}
\begin{center}
{\large{\textbf{\@dedicationtitle}}}
\end{center}
\par
}{\newpage\thispagestyle{empty}\clearpg}
\let\tableofcontents@orig=\tableofcontents
\renewcommand{\tableofcontents}{
\thispagestyle{myheadings}
\tableofcontents@orig
\newpage\thispagestyle{empty}\clearpg
}
\let\listoftables@orig=\listoftables
\renewcommand{\listoftables}{
\thispagestyle{myheadings}
\listoftables@orig
\newpage\thispagestyle{empty}\clearpg
}
\let\listoffigures@orig=\listoffigures
\renewcommand{\listoffigures}{
\thispagestyle{myheadings}
\listoffigures@orig
\newpage\thispagestyle{empty}\clearpg
}
\newenvironment{Notations}{
\thispagestyle{myheadings}
\phantomsection
\addcontentsline{toc}{chapter}{\@notationstoc}
\chapter*{\@notationstitle}
\par
}{\newpage\thispagestyle{empty}\clearpg}
\newenvironment{Abbreviations}{
\thispagestyle{myheadings}
\phantomsection
\addcontentsline{toc}{chapter}{\@abbreviationstoc}
\chapter*{\@abbreviationstitle}
\par
}{\newpage\thispagestyle{empty}\clearpg}
\newenvironment{Main}{
\markboth{}{}
\pagestyle{myheadings}
\pagenumbering{arabic}
\fancypagestyle{plain}{
\fancyhf{}
\fancyhead[R]{\ifnum\thepage=1\relax\else\thepage\fi}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
}
\mainmatter
}{\par\backmatter\clearpg}
\newenvironment{Appendix}{
\pagestyle{myheadings}
\pagenumbering{arabic}
\fancypagestyle{plain}{
\fancyhf{}
\fancyhead[R]{\thepage}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
}
\@mainmattertrue
\appendix
}{\@mainmatterfalse\par\clearpg}
\renewcommand{\citet}[1]{
\textsuperscript{\cite{#1}}
}
\bibliographystyle{IEEEtran}
\let\bibliography@orig=\bibliography
\renewcommand{\bibliography}[1]{
\bibliography@orig{#1}
}
\newcommand{\backcover}{
\clearpg
}
\endinput
%%
%% End of file `uvicthesis.cls'.
