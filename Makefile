# Copyright (C) 2016 by Leyuan Pan <leyuanpan@gmail.com>
#
# This file is part of the UVIC-Thesis package project.
#
# UVIC-Thesis is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# UVIC-Thesis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Installation directory
PREFIX ?= install
# LaTeX path and tools
ifeq ($(OS),Windows_NT)
PATH_LATEX := "/cygdrive/c/Program Files/MiKTeX 2.9/miktex/bin/x64/"
else
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
PATH_LATEX :=
else
PATH_LATEX := /usr/bin/
endif
endif

# template class and package
PACKAGE := uvicthesis
PACKAGE_SRC := $(PACKAGE).ins $(PACKAGE).dtx $(PACKAGE).bib 

# your own thesis, default is sample
SAMPLE := sample
SAMPLE_SRC := $(SAMPLE).tex $(SAMPLE).bib

# LaTeX compilers and tools
CC_LATEX := $(PATH_LATEX)latex
CC_PDFLATEX := $(PATH_LATEX)pdflatex
CC_XELATEX := $(PATH_LATEX)xelatex
CC_BIBTEX := $(PATH_LATEX)bibtex
DVI2PDF := $(PATH_LATEX)dvipdfm
DVI2PS := $(PATH_LATEX)dvips
PS2PDF := $(PATH_LATEX)ps2pdf
MKIDX := $(PATH_LATEX)makeindex
RM := rm -f -r
CP := cp -r
MKDIR := mkdir -p
# compiling flags
FLAG_LATEX := -synctex=1 -src -interaction=nonstopmode
FLAGS_PDFLATEX := -synctex=1 -interaction=nonstopmode
FLAGS_XELATEX := -synctex=1 -interaction=nonstopmode

Q := @
# making rules
all: install

sample: pkg $(SAMPLE).pdf

pkg: $(PACKAGE).pdf

rebuild: clean all

install: pkg
	$(Q)$(MKDIR) $(PREFIX)
	$(Q)$(CP) -v $(PACKAGE).cls $(PREFIX)
	$(Q)$(CP) -v $(PACKAGE).cfg $(PREFIX)
	$(Q)$(CP) -v $(PACKAGE).dtx $(PREFIX)
	$(Q)echo 
	$(Q)echo "Installed to $(PREFIX)"
	$(Q)echo
	$(Q)date

# rules of making package
$(PACKAGE).pdf: $(PACKAGE).dvi
	$(DVI2PS) $(PACKAGE).dvi
	$(PS2PDF) $(PACKAGE).ps
	#$(DVI2PDF) $(PACKAGE).dvi

$(PACKAGE).dvi: $(PACKAGE).dtx $(PACKAGE).cls $(PACKAGE).bbl $(PACKAGE).ind
	$(CC_LATEX) $(FLAG_LATEX) $(PACKAGE).dtx
	$(CC_LATEX) $(FLAG_LATEX) $(PACKAGE).dtx

$(PACKAGE).cls: $(PACKAGE_SRC)
	$(RM) $(PACKAGE).cls $(PACKAGE).cfg
	$(CC_LATEX) $(FLAG_LATEX) $(PACKAGE).ins

$(PACKAGE).idx: $(PACKAGE).dtx
	$(CC_LATEX) $(FLAG_LATEX) $(PACKAGE).dtx

$(PACKAGE).bbl: $(PACKAGE).dtx $(PACKAGE).bib
	$(CC_LATEX) $(FLAG_LATEX) $(PACKAGE).dtx
	$(CC_BIBTEX) $(PACKAGE)

$(PACKAGE).ind: $(PACKAGE).idx
	$(MKIDX) -s gind $(PACKAGE)
	
# rules of making main
$(SAMPLE).pdf: $(SAMPLE).dvi
	$(DVI2PS) $(SAMPLE).dvi
	$(PS2PDF) $(SAMPLE).ps
	#$(DVI2PDF) $(SAMPLE).dvi
	
$(SAMPLE).dvi: $(SAMPLE_SRC) $(PACKAGE).cls $(SAMPLE).bbl
	$(CC_LATEX) $(FLAG_LATEX) $(SAMPLE).tex
	$(CC_LATEX) $(FLAG_LATEX) $(SAMPLE).tex

$(SAMPLE).bbl: $(SAMPLE_SRC)
	$(CC_LATEX) $(FLAG_LATEX) $(SAMPLE).tex
	$(CC_BIBTEX) $(SAMPLE)

$(SAMPLE).ind: $(SAMPLE).tex $(SAMPLE).bbl
	$(MKIDX) -s gind $(SAMPLE)


.PHONY: all sample pkg rebuild install view open clean

view: open

open: $(PACKAGE).pdf
	open $(PACKAGE).pdf

clean:
	$(RM) *.aux *.log *.toc *.ind *.inx *.gls *.glo *.idx *.ilg *.out *.bak *.bbl *.brf *.blg *.dvi *.ps *.synctex.gz* *.lof *.lot
	$(RM) install
	$(RM) $(PACKAGE).pdf $(PACKAGE).cls $(PACKAGE).cfg
	$(RM) $(SAMPLE).pdf

