# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is UVIC-Thesis? ###

* UVIC-Thesis is a LaTeX thesis template package for the University of Victoria supporting the master thesis, the doctoral candidacy proposal and dissertation.
* version 0.3.
* The structure and formatting of this template follows the [requirements](http://www.uvic.ca/graduatestudies/resourcesfor/students/thesis/scope/index.php) provided by UVIC library.
* This project is distributed under the GNU public license version 3 (GPLv3).

### How do I get set up? ###

* Set up instructions

    1. To finish the installation you have to move the following three files into a directory searched by TeX (TEXMF/tex/latex/uvicthesis/ is recommended):

        * uvicthesis.cls
        * uvicthesis.cfg
        * IEEEtran.bst

    2. Or just put these three files in the folder along with your main TeX file.

* Configuration
    
    Follow the user manual in the repository directory (uvicthesis.pdf).

### Contribution guidelines ###

* Not open yet.

### Who do I talk to? ###

* Leyuan Pan: leyuanpan@gmail.com